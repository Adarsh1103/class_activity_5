package com.example.classactivity5;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    String [] teamNames = {"Karachi Kings", "Peshawar Zalmi", "Quetta Gladiators", "Multan Sultan",
                           "Lahore Qalandar", "Islamabad United"};

    int[] teamLogos = {R.drawable.karachi, R.drawable.peshawar, R.drawable.quetta, R.drawable.multan,
                       R.drawable.lahore, R.drawable.islamabad  }  ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = findViewById(R.id.teams);

        // customList acts like customized ArrayAdapter which sets customized listview
        CustomList customList = new CustomList(this, teamNames, teamLogos);
        listView.setAdapter(customList);
    }
}

