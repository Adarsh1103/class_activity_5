package com.example.classactivity5;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class CustomList extends ArrayAdapter {

    String [] teamName;
    int [] teamLogo;
    Context context;

    public CustomList(@NonNull Context context, String [] teamName, int [] teamLogo) {
        super(context, R.layout.custom_list, teamName);
        this.teamLogo = teamLogo;
        this.teamName = teamName;
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        //storing custom_list layout in a view...now all images and text in custom_list are in view
        View view = LayoutInflater.from(context).inflate(R.layout.custom_list, parent, false);
        ImageView imageView = view.findViewById(R.id.teamlogo);
        TextView textView = view.findViewById(R.id.teamname);


        textView.setText(teamName[position]);
        imageView.setImageResource(teamLogo[position]);

        return view;
    }
}
